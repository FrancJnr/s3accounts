<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


$page_security = 'SA_SETUPDISPLAY'; // A very low access level. The real access level is inside the routines.
$path_to_root = "..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

page(_($help_context = "Pending Approvals"), false, false, "", $js);
include 'Report.php';
$rep = new ReportMpdf();
$TB_PREF = "0_";
if(isset($_GET['type'])){
	$data = $rep->invoiceData("",$_GET['type']);
         if(isset($_POST['trans_no'])){
            $data2 = $rep->invoiceData($_POST['trans_no'], $_GET['type']);
            $rep->printReport($data2, $_GET['type']);
         }
}
         
    
?>

<?php

function approval_view($row){
	$id = 0;
	$html='<div class="container-fluid">
   
	<div class="row">
	
	  <div class="col-md-10">
	  <table style="width:100%; background-color:alice-blue;">
	  <tr>
		  <td>
			  <table id="invoices" class="display" style="width:50%;">
			  <thead class="grey lighten-2">
				  <tr>
					  <th scope="col">#</th>
					  <th scope="col">Date</th>
					  <th scope="col">University</th>
					  <th scope="col">Particulars</th>
					  <th scope="col">Commission</th>
					  <th scope="col">Status</th>
					  <th scope="col">Actions</th>
				  </tr>
			  </thead>
			  <tbody>';  	 	 					
							  foreach($row as $myrow){
								  $id+=1;
                                      $status = '<label class="label-danger">Unpaid</label>';
                                      if($myrow['alloc']==$myrow['ov_amount']){
                                        $status = '<label class="label-success">Unpaid</label>';
                                      }
									  
									  $html.= "<tr>";
									  $html.="<td>".$id."</td>";
									  $html.="<td>".$myrow["tran_date"]."</td>";
									  $html.="<td>".$myrow['name']."</td>";
									  $html.="<td>".$myrow['particulars']."</td>";
									  $html.="<td>".$myrow['ov_amount']."</td>";
									  $html.="<td>".$status."</td>";
									  $html.="<td>
                                      <form method='post'>
                                        <input type='hidden' name='trans_no' value='".$myrow['trans_no']."'></input>
                                        <button id='".$myrow['trans_no']."' type='submit' class='btn btn-default'>
                                        <span class='glyphicon glyphicon-download-alt'></span> view
                                        </button>
                                      </form>
                                      </td>";
									  $html.="</tr>";	  

								  }							
				  
			  $html.="</tbody>";
			  $html.="</table>";
			  $html.="</tr>";
	  $html.="</table>";
	  $html.="
	  </div>
	  <div class='col-md-2'>
	  <ul>
		  <li><a href='./?type=comm'>Commission</a></li>
		  <li><a href='./?type=others'>Others</a></li>

	  </ul>
  </div>
</div>
</div>";
echo $html;

	  

}
?>
		
<html lang="en">
  <head>
		<style>
				.error{
					background-color:red;
				}
				.success{
					background-color:green;
				}
		    	#invoices{
					width:100%;
				}
                .inner{
                    display: inline-block;
					width:80px;
           		 }
				
				.sorting{
					color:white;
					margin-left:0px;
				}
				table.dataTable thead tr {
				background-color: green;
				color:white !important;
				}
				#invoices {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
				}

				#invoices td, #invoices th {
				border: 1px solid #fff;
				/* padding: 0px; */
				}

				#invoices tr:nth-child(even){background-color: #90c1e8;}
				#invoices tr:nth-child(odd){background-color: #ddd;}

				#invoices tr:hover {background-color: #ffff;}

				#invoices td, #invoices th {
				border: 1px solid #fff;
				/* padding: 5px; */
				}

				#invoices tr:nth-child(even){background-color: #90c1e8;}
				#invoices tr:nth-child(odd){background-color: #ddd;}

				#invoices tr:hover {background-color: #ffff;}

				#notification {
					position:fixed;
					top:20%;
					width:80%;
					z-index:105;
					text-align:center;
					font-weight:normal;
					font-size:14px;
					font-weight:bold;
					color:white;
					/* background-color:#8be09f; */
					padding:5px;
				}
				#notification span.dismiss {
					border:2px solid #FFF;
					padding:0 5px;
					cursor:pointer;
					float:right;
					margin-right:10px;
				}
				#notification a {
					color:white;
					text-decoration:none;
					font-weight:bold
				}
				.overlay{
					display: none;
					position: fixed;
					width: 100%;
					height: 100%;
					top: 0;
					left: 0;
					z-index: 999;
					background: rgba(255,255,255,0.8) url("/s3accounts/themes/canvas/images/ajax-loader.gif") center no-repeat;
					
				}
				body{
					text-align: center;
				}
				/* Turn off scrollbar when body element has the loading class */
				body.loading{
					overflow: hidden;   
				}
				/* Make spinner image visible when body element has the loading class */
				body.loading .overlay{
					display: block;
				}

		
		</style>

  </head>

  <body class="loading">
		<?php
			approval_view($data);
		?>
  </body>   
	<!-- DataTables -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/s3accounts/assets/DataTables-1.10.23/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="/s3accounts/assets/DataTables-1.10.23/media/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="/s3accounts/assets/bootstrap-3.4.1-dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/s3accounts/assets/bootstrap-3.4.1-dist/css/bootstrap.min.css">

        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/jquery-3.5.1.min.js"></script>
        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/jquery.js"></script>
        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="/s3accounts/assets/DataTables-1.10.23/media/js/dataTables.bootstrap5.min.js"></script>
        <!-- <script>
		$('.btn').on('click', function(e){
			e.preventDefault();
            var data = {};        
			data.trans_no = $(this).attr('id');
			console.log("clicked");

			$.ajax({
				type: "POST",
				url: "",
				data: data,
			}).done(function(data) {
				
						// window.setTimeout(function(){location.reload()},2000)

					
			});
		});

	</script> -->


	<script>
		$(document).ready(function() {
			$('#invoices').DataTable({
				"paging":   false,
				"ordering": true,
				"info":     false,
				"scrollY": 400,
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				],
			
				"autoWidth": false
			});
			
		} );
	
	</script>
	<script>
		$(document).on({
			ajaxStart: function(){
				$("body").addClass("loading"); 
			},
			ajaxStop: function(){ 
				$("body").removeClass("loading"); 
			}    
		});
	
	</script>
		



	<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script> -->
	
 
</html>