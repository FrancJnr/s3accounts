<div class="col-md-3 col-lg-2">
                        <div class="card">
                            <div class="expanel expanel-primary">
                                    <div class="expanel-heading">
                                        <h3 class="expanel-title">Cataloguing Department Reports</h3>
                                    </div>
                                    <div class="expanel-body">
                                    <div class="list-group list-group-transparent mb-0 mail-inbox">
                                            <a href="rep_broker_catalogue.php" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Print Catalogue
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Print Catalogue New Order
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Offers File
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Auction Summary
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Pre Auction eCat
                                            </a>
                                    </div>
                                    </div>
                            </div>
                        
                        </div>
                        <div class="card">
                            <div class="expanel expanel-primary">
                                    <div class="expanel-heading">
                                        <h3 class="expanel-title">Logistic Department Reports</h3>
                                    </div>
                                    <div class="expanel-body">
                                    <div class="list-group list-group-transparent mb-0 mail-inbox">
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Sent Mail
                                            </a>
                                    </div>
                                    </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="expanel expanel-primary">
                                    <div class="expanel-heading">
                                        <h3 class="expanel-title">Warehousing Department Reports</h3>
                                    </div>
                                    <div class="expanel-body">
                                    <div class="list-group list-group-transparent mb-0 mail-inbox">
                                            <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                                <span class="icon mr-3"><i class="fe fe-send"></i></span>Sent Mail
                                            </a>
                                    </div>
                                    </div>
                            </div>
                        </div>
                </div>