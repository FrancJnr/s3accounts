<?php
    $path_to_root = "../";
    require $path_to_root."vendor/autoload.php";
    include 'database_connection.php';

    class ReportMpdf extends Database

    {
        CONST reportname = 'files/rep.pdf';
        public $report_content = "";

        public function printReport($data, $type){
            $this->createReport($this->invoice($data, $type));
            return self::reportname;
        }
        public function createReport($html){
            ob_start();//Enables Output Buffering
            $mpdf = new \Mpdf\Mpdf(['orientation' => 'P', 'tempDir' => __DIR__ . '/files', 	'default_font' => 'dejavusans']);
            $mpdf->WriteHTML($html);
            ob_end_clean();//End Output Buffering
            $mpdf->Output(self::reportname, 'I');

        } 
        public function bankDetails($bankId){
            $sql = "SELECT *FROM ".$this->TB_PREF."bank_accounts WHERE id = ".$bankId;
            $this->query = $sql; 
            return ($this->fetchOne());


        }public function invoiceData($trans_no = '', $type = ''){

            if($trans_no ==''){
                $sql = "SELECT trans.trans_no, trans.reference, dm.address, invoiceno, trans.tran_date, dm.name, 
                (CASE WHEN(mt.category='comm') THEN 
                    group_concat(concat(mt.student_name, ',', student_id, ',', mt.course, ',', mt.date_of_birth) separator ';') 
                ELSE
                    mt.comments
                END)
                AS particulars,
                mt.fees, ov_amount
                FROM ".$this->TB_PREF."debtor_trans trans 
                LEFT JOIN ".$this->TB_PREF."voided voided ON trans.type=voided.type AND trans.trans_no=voided.id
                INNER JOIN ".$this->TB_PREF."debtors_master dm ON trans.debtor_no = dm.debtor_no
                INNER JOIN ".$this->TB_PREF."invoice_meta_data mt ON mt.invoice_id = trans.reference
                INNER JOIN ".$this->TB_PREF."currencies curr ON curr.curr_abrev = dm.curr_code
                WHERE trans.type=10
                AND ISNULL(voided.id) AND category = '".$type."'"." GROUP BY  invoiceno ORDER BY trans.tran_date";

                $this->query = $sql; 
                return $this->executeQuery();
            }else{
                $sql = "SELECT trans.trans_no, trans.reference, curr.curr_symbol, trans.tran_date, dm.name, mt.date_of_birth, invoiceno, dm.address,
                 mt.student_name, mt.bank_details, mt.student_id, mt.course, mt.fees, ov_amount, mt.comments
                FROM ".$this->TB_PREF."debtor_trans trans 
                LEFT JOIN ".$this->TB_PREF."voided voided ON trans.type=voided.type AND trans.trans_no=voided.id
                INNER JOIN ".$this->TB_PREF."debtors_master dm ON trans.debtor_no = dm.debtor_no
                INNER JOIN ".$this->TB_PREF."invoice_meta_data mt ON mt.invoice_id = trans.reference
                INNER JOIN ".$this->TB_PREF."currencies curr ON curr.curr_abrev = dm.curr_code
                WHERE trans.type=10
                AND ISNULL(voided.id) ";
                $sql .=" AND trans_no = '".$trans_no."' AND category = '".$type."'";
                echo $sql;
                $this->query = $sql; 
                return $this->executeQuery();
            }

            

        }
        public function invoiceMetaData($reference){
            $sql = "SELECT trans.trans_no, trans.reference, trans.tran_date, dm.name, invoiceno, dm.address, mt.student_name, mt.bank_details, mt.student_id, mt.course, mt.fees, ov_amount
            FROM ".$this->TB_PREF."debtor_trans trans 
            LEFT JOIN ".$this->TB_PREF."voided voided ON trans.type=voided.type AND trans.trans_no=voided.id
            INNER JOIN ".$this->TB_PREF."debtors_master dm ON trans.debtor_no = dm.debtor_no
            INNER JOIN ".$this->TB_PREF."invoice_meta_data mt ON mt.invoice_id = trans.reference
            WHERE trans.type=10
            AND ISNULL(voided.id) AND invoice_id = '".$reference."
            ORDER BY trans.tran_date";
            $this->query = $sql; 
            return $this->executeQuery();

        }
        public function invoice($data, $type){
        
            $address = $data[0]['address'];
            $invoiceNo = $data[0]['invoiceno'];
            $transDate = date('M, d, yy',strtotime($data[0]['tran_date']));
            $bankId = $data[0]['bank_details'];
            $currencySymbl = $data[0]['curr_symbol'];
            
$html = '<html>
	<head>
		<meta charset="utf-8">
		<title>Invoice</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<header>
			<address contenteditable>
            <table style="border:0px;">
                <tr style="border:0px;  padding:0px;">
                    <td style="border:0px; padding:0px;">
                        <span><img alt="" src="logo_frontaccounting.png"></span>
                    </td>
                    <td style="border:0px;">
                    <h1 class="headtitle" >S Three Education Consultants Limited</h1>
                        <p class="headtext">2nd Floor, Westlands Square (Above Java/Tuskys)</p>
                        <p class="headtext">Ring Road Westlands/ Waiyaki Way</p>
                        <p class="headtext">Nairobi/ Kenya</p>
                        <p class="headtext">T: +254 799 114400/ +254 755 594444</p>
                        <p class="headtext">E:info@s3education.com </p>
                    </td>
                </tr>
            </table>
            </address>
            <address contenteditable>
            <table style="padding-top:20px;">
                <tr style="border:0px;  padding:0px;">
                    <td colspan="5%" style="border:0px;"><pre>'.
                        $address
                    .'</pre></td>
                </tr>
            </table>
            </address>    
		</header>
		<article>
			<table style="width:100%">
				<tr>
					<td style="text-align: center; border:0px;" colspan=100%><b><u>INVOICE</u><b></td>
				</tr>
                <tr>
				</tr>
				<tr>
					<td style="text-align: left; border:0px;" colspan=50%>DATE: '.$transDate.'</td>
                    <td style="text-align: right; border:0px;" colspan=50%>INVOICE#:'.$invoiceNo.'</td>
				</tr>
		
			</table>
			<table style="border:1px;" class="inventory">
				<thead>
					<tr>';
                    if($type == 'comm'){
                       $html.=' <th><span contenteditable>#</span></th>
						<th><span contenteditable>NAME</span></th>
						<th><span contenteditable>STUDENT ID</span></th>
						<th><span contenteditable>DATE OF BIRTH</span></th>
                        <th><span contenteditable>COURSE</span></th>
						<th><span contenteditable>FEE('.$currencySymbl.')</span></th>
                        <th><span contenteditable>COMMISSION('.$currencySymbl.')</span></th>';

                        $html.= '</tr>
                        </thead>
                        <tbody>';
                        $invoiceTotal = 0;
                        $id = 1;
                        foreach($data as $data){
                            $html.='<tr style="border:1px;">
                                <td>'.$id.'</td>
                                <td>'.$data['student_name'].'</td>
                                <td>'.$data['student_id'].'</td>
                                <td>'.$data['date_of_birth'].'</td>
                                <td>'.$data['course'].'</td>
                                <td>'.$data['fees'].'</td>
                                <td>'.$data['ov_amount'].'</td>
                            </tr>';
                            $invoiceTotal=+$data['ov_amount'];
                            $id=+1;

                        }
                            $html.='<tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>'.$currencySymbl."".$invoiceTotal.'</td>
                            </tr>';
                            $html.='
                        </tbody>';

                    }else{
                        $html.=' <th><span contenteditable>#</span></th>
						<th><span contenteditable>Description</span></th>
                        <th><span contenteditable>COMMISSION('.$currencySymbl.')</span></th>';

                        $html.= '</tr>
                        </thead>
                        <tbody>';
                        $invoiceTotal = 0;
                        $id = 1;
                        foreach($data as $data){
                            $html.='<tr style="border:1px;">
                                <td>'.$id.'</td>
                                <td>'.$data['comments'].'</td>
                                <td>'.$data['ov_amount'].'</td>
                            </tr>';
                            $invoiceTotal=+$data['ov_amount'];
                            $id=+1;

                        }
                            $html.='<tr>
                                <td></td>
                                <td></td>
                                <td>'.$currencySymbl."".$invoiceTotal.'</td>
                            </tr>';
                            $html.='
                        </tbody>';

                    }
                    
						

					
			$html.='</table>
			<table style="border:0px;">
				<tr>
					<td colspan=60% style="border:0px;"><b><u>ACCOUNT DETAILS</u><b></td>
				</tr>
				<tr>
                     <td colspan=60% style="border:0px;"><strong>Kindly Make the above payments to the following '.$this->bankDetails($bankId)['bank_curr_code']. ' account:<strong></td>
				</tr>
				<tr>
                      <td colspan=40% style="border:0px;"><strong>Bank Name:<strong></td>
                      <td colspan=60% style="border:0px;"><strong>Diamond Trust Bank(K) Ltd - Bank Code 63<strong></td>
				</tr>
                <tr>
                        <td colspan=40% style="border:0px;"><strong>Branch Name:<strong></td>
                        <td colspan=30% style="border:0px; text-align:left;">
                        <p>
                        <strong>
                        9 West-Branch Code 055 <br>
                        Ground Floor, 9 West Tower, Westlands<br>
                        P.O Box 73631-00200 <br>
                        Nairobi, Kenya <br>
                        <strong>
                        </p></td>
                </tr>
                <tr>
                        <td colspan=40% style="border:0px;"><strong>Account Name:<strong></td>
                        <td colspan=30% style="border:0px; text-align:left;">
                        <p>
                        <strong>
                        S Three Education Consultants Ltd
                        <strong>
                        </p></td>
                </tr>
                <tr>
                <td colspan=40% style="border:0px;"><strong>Account Number:<strong></td>
                <td colspan=30% style="border:0px; text-align:left;">
                    <p>
                    <strong>
                    '.$this->bankDetails($bankId)['bank_account_number'].'
                    <strong>
                    </p></td>
                </tr>
                <tr>
                <td colspan=40% style="border:0px;"><strong>Local Swift Code:<strong></td>
                <td colspan=30% style="border:0px; text-align:left;">
                    <p>
                    <strong>
                    DTKEKENA
                    <strong>
                    </p></td>
                </tr>
			</table>
            <table style="padding-top:50px; border:0px;">
            <tr style="border:0px";>
                <td style="border:0px;">
                <p>_________________________</p>
                <td>
            </tr>
            <tr style="border:0px;">
                <td style="border:0px;">
                <p>Altaf Daya - Director</p>
                <td>
            </tr>
            </table>
            <table style="padding-top:60px; border:0px;">
                <tr style="border:0px;padding-top:100px;">
                <td style="border:0px;">
                    <p>____________________________________________________________________________________________________</p>
                <td>
                <td style="border:0px;">
                    <img style="height:50px; width:50px;"  src="footer.png">
                <td>
                </tr>
            </table>
		</article>
	</body>
</html>';

          return $html;


        }   
    }

    ?>