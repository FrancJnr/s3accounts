<?php
class Database{
  
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "techsava_s3education";
    private $username = "root";
    private $password = "";
    public $conn;
    public $validToken = false;
    public $TB_PREF = "0_";
    public $query;
  
    // get the database connection
    public function getConnection(){
  
        $this->conn = null;
  
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
  
        return $this->conn;
    }
    public function executeQuery(){
      try {
        $rows = $this->getConnection()->query($this->query)->fetchAll();
        return $rows;
        echo $this->query;
      } catch (Exception $ex) {
          var_dump($ex);
      }
        
    }
    public function fetchOne(){
        try {
          $rows = $this->getConnection()->query($this->query)->fetch(PDO::FETCH_ASSOC);
          return $rows;
        } catch (Exception $ex) {
            var_dump($ex);
        }
          
      }
}
?>