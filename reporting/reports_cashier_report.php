<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


	$page_security = 'SA_SETUPDISPLAY'; // A very low access level. The real access level is inside the routines.
	$path_to_root = "..";

	include_once($path_to_root . "/includes/session.inc");
	include_once($path_to_root . "/includes/ui.inc");
	include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
	include_once($path_to_root . "/includes/date_functions.inc");
	include_once($path_to_root . "/includes/data_checks.inc");
	include_once($path_to_root . "/gl/includes/gl_db.inc");
    page(_($help_context = "Cashier Report"), false, false, "", $js);

	
?>
<?php
	$base_url = explode('?', $_SERVER[REQUEST_URI], 2)[1];
	$paramC = explode('&', $base_url , 2)[0];
	$paramD = explode('&', $base_url , 2)[1];

	  $cashierId = 1;
	  if ($_GET['C'] !==null){
		$cashierId = $_GET['C'];
		
	 }
	 $date = date("Y-m-d");// current date

	 if ($_GET['D'] !==null){
		$date = $_GET['D'];
		if($date == 'today'){
			$date = date("Y-m-d");
		}
		
	 }
	 $cashiers = get_cashiers();

	 $trans = get_cashier_transactions($trans_no=0, $date, $memo='', $cashierId);
	 $rows = db_num_rows($trans);


?>

	<html lang="en">
  <head>
		<style>
			.dt-button{
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				padding: 15px 32px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 12px;
				
			}
			.sorting{
					color:white;
					margin-left:0px;
				}
				table.dataTable thead tr {
				background-color: black;
				color:white !important;
				}
				#dailySales {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
				}

				#dailySales td, #dailySales th {
				border: 1px solid #fff;
				/* padding: 0px; */
				}

				#dailySales tr:nth-child(even){background-color: #90c1e8;}
				#dailySales tr:nth-child(odd){background-color: #ddd;}

				#dailySales tr:hover {background-color: #ffff;}

				#dailySales td, #dailySales th {
				border: 1px solid #fff;
				/* padding: 5px; */
				}

				#dailySales tr:nth-child(even){background-color: #90c1e8;}
				#dailySales tr:nth-child(odd){background-color: #ddd;}

				#dailySales tr:hover {background-color: #ffff;}

				#notification {
					position:fixed;
					top:20%;
					width:80%;
					z-index:105;
					text-align:center;
					font-weight:normal;
					font-size:14px;
					font-weight:bold;
					color:white;
					background-color:#8be09f;
					padding:5px;
				}
				#notification span.dismiss {
					border:2px solid #FFF;
					padding:0 5px;
					cursor:pointer;
					float:right;
					margin-right:10px;
				}
				#notification a {
					color:white;
					text-decoration:none;
					font-weight:bold
				}
				#cashiers{
					color:#1275ae;
					background-color:black;
					font-weight:bold;
				}
				#cashiers li a{
					color:white;
					width:50px;
					padding-left:20%;
				}

	
		
		</style>

  </head>

  <body>

    <div class="container-fluid">
   
      <div class="row">

        <div class="col-md-9">
		<table style="width:100%; background-color:alice-blue;">
		<tr>
			<td>
				<table id="dailySales" class="display" style="width:50%;">
				<thead class="grey lighten-2">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Ref</th>
					<th scope="col">Date</th>
					<th scope="col">Payment Mode</th>
					<th scope="col">Description</th>
					<th scope="col">Account Name</th>
					<th scope="col">CASH IN</th>
					<th scope="col">CASH OUT</th>
					<th scope="col">Balance</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($rows > 0){
							$id = 0;
							$total = 0;
							$balance = 0.0;
							$cashout = 0.0;							
								while ($myrow=db_fetch($trans)){
									$id+=1;
									if ((($myrow['type']) == 12 || $myrow['type'] == 2) && ($myrow['amount']>0.0)){
										if($myrow['mode_']=='cheque'){$payment_type='CHQ-'.$myrow['cheque_no'];}else if($myrow['mode_']=='cash'){$payment_type='Cash';}else{$payment_type='Paybill-'.$myrow['paybill_ref'];}
										$total += $myrow['amount'];
										$balance = $total;
										$trow = '<tr>';
										$trow.='<td>'.$id.'</td>';
										$trow.='<td>'.$myrow['reference'].'</td>';
										$trow.='<td>'.$myrow["tran_date"].'</td>';
										$trow.='<td>'.$payment_type.'</td>';
										$trow.='<td>'.$myrow['person_name'].'-'.$myrow['memo_'].'</td>';
										$trow.='<td>'.$myrow['account_name'].'</td>';
										$trow.='<td>'.abs($myrow['amount']).'</td>';
										$trow.='<td>-</td>';
										$trow.='<td><b>'.abs($balance).'</b></td>';
										$trow.='</tr>';

										echo $trow;


									}
									if ((($myrow['type']) == 1 || $myrow['type'] == 4) && ($myrow['amount']>0.0)){
										$cashout += $myrow['amount'];
										$balance = $balance-$cashout;
										$trow = '<tr>';
										$trow.='<td>'.$id.'</td>';
										$trow.='<td>'.$myrow['reference'].'</td>';
										$trow.='<td>'.$myrow["tran_date"].'</td>';
										$trow.='<td>'.$myrow["payment_type"].'</td>';
										$trow.='<td>'.$myrow['person_name'].'-'.$myrow['memo_'].'</td>';
										$trow.='<td>'.$myrow['account_name'].'</td>';
										$trow.='<td>-</td>';
										$trow.='<td>'.abs($myrow['amount']).'</td>';
										$trow.='<td>'.abs($balance).'</td>';
										$trow.='</tr>';
										echo $trow;


									}
									// if ($myrow['amount'] < 0.0)
									// else
									// 	$trow.='<td></td>';	
									
								}

								
						}
						?>
					
				</tbody>
				</table>
		</tr>
		</table>



        </div><!-- /.blog-main -->

        <div id="cashiers" class="col-md-2 col-sm-offset-1 blog-sidebar">
		<div class="sidebar-module">
            <h4>Cashiers</h4>
            <ol class="list-unstyled">
			<?php  
				while ($myrow2=db_fetch($cashiers)){
					echo '<li><a href="reports_cashier_report.php?C='.$myrow2['id'].'&D=today">'.$myrow2['real_name'].'</a></li>';

				}    
			  ?>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Dates</h4>
            <ol class="list-unstyled">
			<?php 
					 $today = date("Y-m-d");// current date

					 $yesterday= date('Y-m-d',strtotime("-1 days"));

					 echo '<li><a href="reports_cashier_report.php?'.$paramC.'&D='.$today.'">Today</a></li>';
					 echo '<li><a href="reports_cashier_report.php?'.$paramC.'&D='.$yesterday.'">Yesterday</a></li>';
					 for($i=0; $i<12; $i++){
						$day = strtotime(date("Y-m-d", strtotime($yesterday)) ."-" .$i. " day");
						echo '<li><a href="reports_cashier_report.php?'.$paramC.'&D='.date('Y-m-d', $day).'">'.date('Y-m-d', $day).'</a></li>';

						
					 }
			?>
            </ol>
          </div>
         
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->
		<link rel="stylesheet" href="/erp/assets/DataTables-1.10.23/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="/erp/assets/DataTables-1.10.23/media/css/dataTables.bootstrap4.min.css">
	    <link rel="stylesheet" href="/erp/assets/bootstrap-3.4.1-dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/erp/assets/bootstrap-3.4.1-dist/css/bootstrap.min.css">

        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery-3.5.1.min.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/dataTables.bootstrap5.min.js"></script>


	<script>
	$(document).ready(function() {
			$('#dailySales').DataTable( {
				"paging":   false,
				"ordering": true,
				"info":     false,
				"scrollY": 400,
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			} );
			
		} );
	
	</script>
	
  </body>
</html>