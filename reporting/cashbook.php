<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


	$page_security = 'SA_SETUPDISPLAY'; // A very low access level. The real access level is inside the routines.
	$path_to_root = "..";

	include_once($path_to_root . "/includes/session.inc");
	include_once($path_to_root . "/includes/ui.inc");
	include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
	include_once($path_to_root . "/includes/date_functions.inc");
	include_once($path_to_root . "/includes/data_checks.inc");
	include_once($path_to_root . "/gl/includes/gl_db.inc");
    page(_($help_context = "CASH BOOK"), false, false, "", $js);

	
?>
<?php
	$base_url = explode('?', $_SERVER[REQUEST_URI], 2)[1];
	$paramC = explode('&', $base_url , 2)[0];
	$paramD = explode('&', $base_url , 2)[1];

	  $cashierId = 1;
	  if ($_GET['C'] !==null){
		$cashierId = $_GET['C'];
		
	 }
	 $date = date("Y-m-d");// current date

	 if ($_GET['D'] !==null){
		$date = $_GET['D'];
		if($date == 'today'){
			$date = date("Y-m-d");
		}
		
	 }
	 $cashiers = get_cashiers();

	 $trans = get_cashier_transactions($trans_no=0, $date, $memo='', 'admin');
	 $rows = db_num_rows($trans);

	//  var_dump(db_fetch($trans));

	//  var_dump(db_fetch($cashiers));

?>

	<html lang="en">
  <head>
  
        <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

		<style>
			.dt-button{
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				padding: 15px 32px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 12px;
				
			}
		
		</style>
 
        <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
  </head>

  <body>

    <!-- <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="#">Home</a>
          <a class="blog-nav-item" href="#">New features</a>
          <a class="blog-nav-item" href="#">Press</a>
          <a class="blog-nav-item" href="#">New hires</a>
          <a class="blog-nav-item" href="#">About</a>
        </nav>
      </div>
    </div> -->

    <div class="container-fluid">
   
      <div class="row">

        <div class="col-md-8">
		<table style="width:100%; background-color:alice-blue;">
		<tr>
			<td>
				<table id="dailySales" class="display" style="width:50%;">
				<thead class="grey lighten-2">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Ref</th>
					<th scope="col">Date</th>
					<th scope="col">Description</th>
					<th scope="col">Account Name</th>
					<th scope="col">DR</th>
					<th scope="col">CR</th>
					<th scope="col">Balance</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($rows > 0){
							$id = 0;
							$total = 0;
							$balance = 0.0;
							$cashout = 0.0;
							var_dump(db_fetch($trans));
							
								while ($myrow=db_fetch($trans)){
									$id+=1;
									if ((($myrow['type']) == 12 || $myrow['type'] == 2) && ($myrow['amount']>0.0)){
										$total += $myrow['amount'];
										$balance = $total;
										$trow = '<tr>';
										$trow.='<td>'.$id.'</td>';
										$trow.='<td>'.$myrow['reference'].'</td>';
										$trow.='<td>'.$myrow["tran_date"].'</td>';
										$trow.='<td>'.$myrow['person_name'].'-'.$myrow['memo_'].'</td>';
										$trow.='<td>'.$myrow['account_name'].'</td>';
										$trow.='<td>'.abs($myrow['amount']).'</td>';
										$trow.='<td>-</td>';
										$trow.='<td><b>'.abs($balance).'</b></td>';
										$trow.='</tr>';
										echo $trow;


									}
									if ((($myrow['type']) == 1 || $myrow['type'] == 4) && ($myrow['amount']>0.0)){
										$cashout += $myrow['amount'];
										$balance = $balance-$cashout;
										$trow = '<tr>';
										$trow.='<td>'.$id.'</td>';
										$trow.='<td>'.$myrow['reference'].'</td>';
										$trow.='<td>'.$myrow["tran_date"].'</td>';
										$trow.='<td>'.$myrow['person_name'].'-'.$myrow['memo_'].'</td>';
										$trow.='<td>'.$myrow['account_name'].'</td>';
										$trow.='<td>-</td>';
										$trow.='<td>'.abs($myrow['amount']).'</td>';
										$trow.='<td>'.abs($balance).'</td>';
										$trow.='</tr>';
										echo $trow;


									}
									// if ($myrow['amount'] < 0.0)
									// else
									// 	$trow.='<td></td>';	
									
								}

								
						}
						?>
					
				</tbody>
				</table>
		</tr>
		</table>



        </div><!-- /.blog-main -->

        <div class="col-md-2 col-sm-offset-1 blog-sidebar">
		<div class="sidebar-module">
            <h4>Category</h4>
            <ol class="list-unstyled">
			<?php  
				echo '<li><a href="cashbook.php?type=cash">Bank</a></li>
					  <li><a href="cashbook.php?type=cash">Cash</a></li>';
			  ?>
            </ol>
          </div>
    
         
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

	<script>
	$(document).ready(function() {
			$('#dailySales').DataTable( {
				"paging":   false,
				"ordering": true,
				"info":     false,
				"scrollY": 400,


				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			} );
			
		} );
	
	</script>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script> -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
	
  </body>
</html>